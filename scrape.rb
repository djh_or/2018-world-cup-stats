#!/usr/bin/env ruby

# Scrapes statistics for the 2018 World Cup games from FIFA.
# Note that a lot of content is loaded dynamically after page load, so I just saved the root dom element to games/n
# where n is the game number. These are included in the repo in a tgz.

require 'nokogiri'
require 'csv'

headers = ["Game", "Group", "Team", "Opponent", "Home/Away", "Score", "WDL", "Pens?", "Goals For", "Goals Against", "Pen Shootout For", "Pen Shootout Against",
           "Attempts", "On-Target",
           "Off-Target", "Blocked", "Woodwork", "Corners", "Offsides", "Ball possession %", "Pass Accuracy %",
           "Passes", "Passes Completed", "Distance Covered km", "Balls recovered", "Tackles", "Blocks", "Clearances",
           "Yellow cards", "Red Cards", "Second Yellow Card leading to Red Card", "Fouls Committed"]
CSV(STDOUT) do |out|
  out << headers
  Dir.glob('games/[0-9]*').sort.each do |f|
    game = f.match(/games\/(?<game>[[:digit:]]+)\z/)[:game]
    doc = Nokogiri::HTML(open(f))
    group_el = doc.at_xpath('//span[contains(@class, "fi__info__group")]')
    if group_el.text.match(/Group[[:space:]](?<group>[[:alpha:]])/)
      group = Regexp.last_match[:group]
      group_stage = true
    else
      group = group_el.text.match(/\A(?<round>[^,]+)/)[:round]
      group_stage = false
    end
    scores = doc.xpath('//span[contains(@class, "fi-s__scoreText")]').text.match(/(?<home>[[:digit:]]+)-(?<away>[[:digit:]]+)/)
    data = doc.xpath('//tr[contains(@class, "table__stats-progress-bar__data")]')
           .map { |e| e.xpath('.//td').map { |e| e.text.strip.sub(/%\z/, '').sub(/[[:space:]]km\z/, '') } }
    %w(home away).zip([0, 2]) do |ha, i|
      team = doc.at_xpath(%Q(//div[@class="team-#{ha}__inner-helper"]/span)).text
      opponent = doc.at_xpath(%Q(//div[@class="team-#{ha == 'home' ? 'away' : 'home'}__inner-helper"]/span)).text
      for_against = (ha == 'home' ? [scores[:home], scores[:away]] : [scores[:away], scores[:home]]).map!(&:to_i)
      row = [game, group, team, opponent, ha, "#{scores[:home]}-#{scores[:away]}"]
      if group_stage
        row.concat [(for_against[0] == for_against[1] ? 'D' : (for_against[0] < for_against[1] ? 'L' : 'W')), '']
        pens_for_against = ['', '']
      else
        if for_against[0] == for_against[1]
          pens = doc.xpath('//span[@class="fi-mu__reasonwin-text"]').text.strip.split("\n").first.match(/\((?<home>[[:digit:]]+)[[:space:]]+-[[:space:]]+(?<away>[[:digit:]]+)/)
          pens_for_against = (ha == 'home' ? [pens[:home], pens[:away]] : [pens[:away], pens[:home]]).map!(&:to_i)
          row.concat [pens_for_against[0] < pens_for_against[1] ? 'L' : 'W', 'y']
        else
          row.concat [for_against[0] < for_against[1] ? 'L' : 'W', '']
          pens_for_against = ['', '']
        end
      end
      row.concat for_against
      row.concat pens_for_against
      row.concat data[1..-1].map { |e| e[i] }
      out << row
    end
  end
end
