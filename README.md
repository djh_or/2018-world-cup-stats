# What
Scrapes fifa.com statistics pages and generates CSV for each team/game.

Most of the content on these pages is dynamically loaded, so root DOM outer text was saved for each page manually.
Note that the game number is the order they appear on https://www.fifa.com/worldcup/matches but officially they may have been given a different number.

These are in `games.tgz`, which must be unpacked to run, although running is not necessary as the output csv is in `world_cup_2018_stats.csv`.

## Columns:
- Game
- Group
- Team
- Opponent
- Home/Away
- Score
- WDL
- Pens?
- Goals For
- Goals Against
- Pen Shootout For
- Pen Shootout Against
- Attempts
- On-Target
- Off-Target
- Blocked
- Woodwork
- Corners
- Offsides
- Ball possession %
- Pass Accuracy %
- Passes
- Passes Completed
- Distance Covered km
- Balls recovered
- Tackles
- Blocks
- Clearances
- Yellow cards
- Red Cards
- Second Yellow Card leading to Red Card
- Fouls Committed

## Running
Games need to be unpacked:
```bash
tar xvzf games.tgz
```

Just run:
```bash
./scrape
```
Only take a couple of seconds to run.
